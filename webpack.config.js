// const webpack = require('webpack');
// const path = require('path');

module.exports = {
    mode: 'development',
    entry: ['./app/App.js'],
    output: {
        path: __dirname + '/dist',
        filename: "[name].bundle.js",
    },
    devServer: {
        historyApiFallback: true,
      },
    devtool: 'source-map',
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: { test: /[\\/]node_modules[\\/]/, name: "vendors", chunks: "initial",reuseExistingChunk: true }
            },
            name:false
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader:'babel-loader',
                query: {
                    presets: ['react','es2015']
                }
            },
            {
                test: /\.(s*)css/,
                exclude: /node_modules/,
                loaders: [
                    'style-loader?sourceMap',
                    'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
                    'sass-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
                ]
            },
            {
                test: /\.(PNG|png|jpg)$/,
                exclude: /node_modules/,
                loader: 'url-loader'
            }
        ]
    }
}
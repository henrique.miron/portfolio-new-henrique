import React,{Component} from 'react';
import TopBar from '../../components/TopBar/TopBar';
import {Col, Row, Grid, Thumbnail, Button, Image, FieldGroup, Checkbox, Radio, FormGroup, FormControl, ControlLabel, Form} from 'react-bootstrap';
import StyleTopBar from './../../components/TopBar/TopBar.scss';
import People from './../../../src/people';
import MyHabilities from '../../components/MyHabilities/MyHabilities';
import StyleArtigos from './../../components/Artigos/SectionArtigos/SectionArtigos.scss';
import StyleQuemSomos from './../../pages/quemSomosInterno/quemSomosInterno.scss';
import StyleFooter from './../../sections/Footer/Footer.scss';
import StyleNews from './../../components/Newsletter/Newsletter.scss';
import CardArticles from '../../components/Cards/CardArticles';

export default class quemSomosInterno extends Component {

    componentDidMount(){
        console.log('People',People);
    }

    render(){
        var habilities = [{name : 'organizado', percent: 10, context:"red" }, {name : 'ativo', percent: 20 }];
        return(
            <div>
                <section className={StyleTopBar.BgRed}>
                    <div class={'full_content'}>
                        <div class={'navBorder'}></div>
                        <TopBar /> 
                    </div>
                </section>
                <section>
                    <Grid>
                        <Row>
                            <Col md={6}>
                                <MyHabilities habilities={habilities} />
                            </Col>
                            <Col md={6}>
                                <MyHabilities habilities={habilities} />
                            </Col>
                        </Row>
                    </Grid>
                </section>
                <section className={StyleArtigos.BgArtigos}>
                    <Grid>
                        <Row>
                            <Col md={12}>
                                <h2>Artigos escritos por mim</h2>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid>
                        <Row>
                            <CardArticles title="Teste" />
                            <CardArticles title="Batata" />
                            <CardArticles title="Cenoura" />

                        </Row>
                    </Grid>
                </section>
                <section className={StyleQuemSomos.BgSomos}>
                    <Grid>
                        <Row className={StyleQuemSomos.FlexAlign}>
                            <Col md={6}>
                                <h2>Quer conhecer os outros Pixels? É só clicar neles ai do lado!</h2>
                            </Col>
                            <Col md={2} className={StyleQuemSomos.ContentSomos}>
                               <Image src="../assets/pixel-thales.png" responsive />;
                               <p><b>Thales Gazaneo</b></p>
                               <span>Diretor de Criação</span>
                            </Col>
                            <Col md={2} className={StyleQuemSomos.ContentSomos}>
                                <Image src="../assets/pixel-vine.png" responsive />;
                                <p><b>Vinicius Rangel</b></p>
                               <span>Diretor de Tecnologia</span>
                            </Col>
                            <Col md={2} className={StyleQuemSomos.ContentSomos}>
                                <Image src="../assets/pixel-miron.png" responsive />;
                                <p><b>Henrique Miron</b></p>
                               <span>Diretor de Operações</span>
                            </Col>
                        </Row>                        
                    </Grid>
                    <Grid className={StyleNews.GridNews}>
                        <div className={StyleNews.BgNews}>
                            <Row>
                                <Col md={12}>
                                    <h3>Assine nossa newsletter para receber descontos e novidades!</h3>
                                </Col>                                
                            </Row>  
                            <Row>
                                <Col md={6} mdOffset={3}>
                                    <Form className={'row'}>
                                      <FormGroup controlId="formInlineName" className={StyleNews.FormGroup+' col-md-6'}>
                                        <FormControl type="text" placeholder="Ex: vine-surfistinha@hotmail.com" className={StyleNews.FormControl}/>
                                      </FormGroup>{' '}
                                      <FormGroup className={'col-md-6'}>
                                        <Button type="submit" block>Quero Receber</Button>
                                      </FormGroup>{' '}
                                    </Form>;
                                </Col>                                
                            </Row>  
                        </div>                      
                    </Grid>
                </section>
                <footer>
                    <Grid>
                        <Row>
                            <Col md={3}>
                                <h5>Contato</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="tel:11961848388"><Image src="../assets/ico-phone.png"/>(11) 96184-8388</a></li>
                                    <li><a href="mailto:4p@ad4pixels.com.br"><Image src="../assets/ico-mail.png"/>4p@ad4pixels.com.br</a></li>
                                    <li><Image src="../assets/ico-marker.png"/>R. Henrique Sertório, 564 - Sala 503 Tatuapé, São Paulo - SP CEP: 03066-065</li>
                                </ul>
                            </Col>
                            <Col md={3}>
                                <h5>Principais Serviços</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="#">Marketing digital</a></li>
                                    <li><a href="#">Criação de identidade visual</a></li>
                                    <li><a href="#">Terceirização estratégica</a></li>
                                    <li><a href="#">Criação de loja virtual</a></li>
                                    <li><a href="#">Desenvolvimento de sistemas</a></li>
                                    <li><a href="#">Desenvolvimento de aplicativos</a></li>
                                    <li><a href="#">Criação de websites</a></li>
                                </ul>
                            </Col>
                            <Col md={3}>
                                <h5>Sobre Nós</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="#">Sobre a 4Pixels</a></li>
                                    <li><a href="#">Equipe</a></li>
                                    <li><a href="#">Principais Cases</a></li>
                                </ul>
                            </Col>
                            <Col md={3}>
                                <h5>Links Úteis</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="#">Suporte</a></li>
                                    <li><a href="#">English Version</a></li>
                                    <li><a href="#">Facebook</a></li>
                                    <li><a href="#">Linkedin</a></li>
                                    <li><a href="#">Contato</a></li>
                                </ul>
                            </Col>
                        </Row>                        
                    </Grid>
                    <Grid>
                        <Row className={'text-center'}>
                            <Col md={12}>
                                <Image src="../assets/logo-footer.png"/>
                            </Col>
                        </Row>
                    </Grid>
                </footer>
            </div>
        );
    }
}
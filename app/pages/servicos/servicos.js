import React,{Component} from 'react';
import StyleServicos from './servicos.scss';
import {Col, Row, Grid, Thumbnail, Button, Image, FieldGroup, Checkbox, Radio, FormGroup, FormControl, ControlLabel, Form} from 'react-bootstrap';
import MainMenu  from '../../components/Menu/MainMenu/MainMenu';
import StyleNews from '../../components/Newsletter/Newsletter.scss';
import ButtonGradient from '../../components/Button/ButtonGradient';
import CardServices from '../../components/Cards/CardServices/CardServices.js';

export default class servicos extends Component{
    render(){
        return(
            <div>
                <section>
                    <Grid className={StyleServicos.Bg}>
                        <Row>
                            <Col md={8} mdOffset={2}>
                                <MainMenu/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} md={5} mdOffset={2} className={StyleServicos.title}>
                                <h1>Criação de indentidade visual</h1>
                                <p>
                                    A indentidade visual é essencial para uma empresa de sucesso, 
                                    pois ela reflete sua marca, os valores de sua empresa 
                                    e toda experiência do seu cliente ao usar seus produtos e serviços
                                </p>
                            </Col>
                        </Row>
                    </Grid>
                    {/* <Grid>
                        <Row>
                            <Col xs={12} md={8} className={StyleServicos.title}>
                                <h1>Criação de indentidade visual</h1>
                                <p>
                                    A indentidade visual é essencial para uma empresa de sucesso, 
                                    pois ela reflete sua marca, os valores de sua empresa 
                                    e toda experiência do seu cliente ao usar seus produtos e serviços
                                </p>
                            </Col>
                        </Row>
                    </Grid> */}
                    <Grid>
                        <Row>
                            <Col md={3} className={StyleServicos.orcamento}>
                                <h3>
                                    Confira o que
                                    Fazemos de Melhor
                                </h3>
                                <ButtonGradient color="Amarelo" texto="Entrar em Contato!" type="submit" block/>
                            </Col>
                            <CardServices title="Criação de Logotipos"/>
                            <CardServices title="Criação de cartões de visita"/>
                            
                        </Row>
                    </Grid>
                    <Grid className={StyleNews.GridNews}>
                        <div className={StyleNews.BgNews+" "+StyleNews.corRoxa}>
                            <Row>
                                <Col md={12}>
                                    <h3>Assine nossa newsletter para receber descontos e novidades!</h3>
                                </Col>                                
                            </Row>  
                            <Row>
                                <Col md={6} mdOffset={3}>
                                    <Form className={'row'}>
                                      <FormGroup controlId="formInlineName" className={StyleNews.FormGroup+' col-md-6'}>
                                        <FormControl type="text" placeholder="Ex: vine-surfistinha@hotmail.com" className={StyleNews.FormControl}/>
                                      </FormGroup>{' '}
                                      <FormGroup className={'col-md-6'}>
                                        <Button type="submit" block>Quero Receber</Button>
                                      </FormGroup>{' '}
                                    </Form>;
                                </Col>                                
                            </Row>  
                        </div>                      
                    </Grid>
                </section>
                <footer>
                    <Grid>
                        <Row>
                            <Col md={3}>
                                <h5>Contato</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="tel:11961848388"><Image src="../assets/ico-phone.png"/>(11) 96184-8388</a></li>
                                    <li><a href="mailto:4p@ad4pixels.com.br"><Image src="../assets/ico-mail.png"/>4p@ad4pixels.com.br</a></li>
                                    <li><Image src="../assets/ico-marker.png"/>R. Henrique Sertório, 564 - Sala 503 Tatuapé, São Paulo - SP CEP: 03066-065</li>
                                </ul>
                            </Col>
                            <Col md={3}>
                                <h5>Principais Serviços</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="#">Marketing digital</a></li>
                                    <li><a href="#">Criação de identidade visual</a></li>
                                    <li><a href="#">Terceirização estratégica</a></li>
                                    <li><a href="#">Criação de loja virtual</a></li>
                                    <li><a href="#">Desenvolvimento de sistemas</a></li>
                                    <li><a href="#">Desenvolvimento de aplicativos</a></li>
                                    <li><a href="#">Criação de websites</a></li>
                                </ul>
                            </Col>
                            <Col md={3}>
                                <h5>Sobre Nós</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="#">Sobre a 4Pixels</a></li>
                                    <li><a href="#">Equipe</a></li>
                                    <li><a href="#">Principais Cases</a></li>
                                </ul>
                            </Col>
                            <Col md={3}>
                                <h5>Links Úteis</h5>
                                <ul className={'list-unstyled'}>
                                    <li><a href="#">Suporte</a></li>
                                    <li><a href="#">English Version</a></li>
                                    <li><a href="#">Facebook</a></li>
                                    <li><a href="#">Linkedin</a></li>
                                    <li><a href="#">Contato</a></li>
                                </ul>
                            </Col>
                        </Row>                        
                    </Grid>
                    <Grid>
                        <Row className={'text-center'}>
                            <Col md={12}>
                                <Image src="../assets/logo-footer.png"/>
                            </Col>
                        </Row>
                    </Grid>
                </footer>
                </div>
        );
    }
}
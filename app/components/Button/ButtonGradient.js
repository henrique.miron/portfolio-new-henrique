import React,{Component} from 'react'
import StyleGradient from './ButtonGradient.scss'
import {Button} from 'react-bootstrap';

export default class ButtonGradient extends Component{

    constructor(props) {
        super(props);
      }

    componentDidMount(){
        document.querySelector('button').onmousemove = (e) => {
            const x = e.layerX
            const y = e.layerY
            e.target.style.setProperty('--x', `${ x }px`)
            e.target.style.setProperty('--y', `${ y }px`)
          }
    }
    render(){
        return(
            <div>
                <Button type={this.props.type} className={StyleGradient.Style+" "+StyleGradient+this.props.color}>
                    <span>{this.props.texto}</span>
                </Button>
            </div>
        )
    }
}
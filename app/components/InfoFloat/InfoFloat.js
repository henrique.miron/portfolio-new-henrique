import React,{Component} from 'react';
import {Col} from 'react-bootstrap';
import StyleInfoFloat from './InfoFloat.scss';

export default class InfoFloat extends Component{
    render(){
        return(
            <div className={StyleInfoFloat.position}>
                <span><img src={require('../../../assets/ico-phone.png')}/>   (11) 1111-111</span>
                <span><img src={require('../../../assets/ico-mail.png')}/> 4p@ad4pixels.com.br</span>
                <span><img src={require('../../../assets/ico-marker.png')}/>  Segunda a Sexta<br/> 09:00h ás 18:00h</span>
            </div>
        )
    }
}
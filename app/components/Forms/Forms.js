import React,{Component} from 'react';
import {Col, Row, Grid, Thumbnail, Button, Image, FieldGroup, Label, Input, FormGroup, FormControl, ControlLabel, Form} from 'react-bootstrap';
import Style from './Forms.scss';
import ButtonGradient from '../../components/Button/ButtonGradient';
import {BroserRouter, Route} from 'react-router-dom'

export default class Forms extends Component {
	constructor(props) {
		super(props);
		this.state = {value: '', username:''};
		
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	  }

	handleChange(e) {
		this.setState({[e.target.name]: e.target.value});
	}

	handleSubmit(e) {
		e.preventDefault();
		console.log(this.state.value)
		const { nome, email } = this.state;
		const data = [nome, email];
		
		fetch("/pixel-portfolio", {
            method: 'GET',
            headers: {'Content-Type': 'application/json'},
            body: data
        }).then(function(response) {
            if (response.status >= 400) {
              throw new Error("Bad response from server");
            }
            return response.json();
        }).then(function(data) {
            console.log(data)    
            if(data == "success"){
               this.setState({msg: "Thanks for registering"});  
            }
        }).catch(function(err) {
            console.log(err)
        });
	  }

	render() {
		return (
					<form className={Style.style} onSubmit={this.handleSubmit}>
						<Col xs={12} className="text-center">
							<h4>Nos envie uma mensagem!</h4>
							<h6>Agora que você conheceu nossos trabalhos deve estar curioso para saber como podemos te ajudar. Muito simples, preencha o formulário abaixo ou então entre em contato por uma das formas ao lado.</h6>
						</Col>
						<Col xs="12">
							<FormGroup role="form">
								<ControlLabel for="nome">Seu Nome</ControlLabel>
								<FormControl type="text" name="username" placeholder="Ex: Jõao Manuel da Silva" onChange={this.handleChange}/>
							</FormGroup>
							<FormGroup>
								<ControlLabel for="nome">Seu Email</ControlLabel>
								<FormControl type="text" name="email" placeholder="Ex: vine-surfistinha@hotmail.com" onChange={this.handleChange}/>
							</FormGroup>
							<FormGroup>
								<ControlLabel for="nome">Seu Telefone</ControlLabel>
								<FormControl type="text" name="telefone" placeholder="Ex: (11) 2091-5555" onChange={this.handleChange}/>
							</FormGroup>
							<FormGroup>
								<ControlLabel for="nome">Sua Mensagem</ControlLabel>
								<textarea className="form-control" type="textarea" name="mensagem" rows="4" cols="50" placeholder="Olá, quero fazer um site!" onChange={this.handleChange}/>
							</FormGroup>
							<ButtonGradient texto="Entrar em Contato!" type="submit" block/>
						</Col>
					</form>
			)
	}
}
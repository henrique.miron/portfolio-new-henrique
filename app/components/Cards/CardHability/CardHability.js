import React,{Component} from 'react';
import {ProgressBar, Button, Col, Row} from 'react-bootstrap';
import StyleCard from './CardHability.scss';
import { bootstrapUtils } from 'react-bootstrap/lib/utils';
bootstrapUtils.addStyle(ProgressBar, 'red');


export default class CardHability extends Component {
    
    render(){
        var customProgressBarStyle = (
            <div>
              <style type="text/css">{`
              .progress-bar-red {
                  background-image: linear-gradient(to right top, #bc3d20, #b63b1f, #af381d, #a9361c, #a3341b); !important;
              }
              `}</style>
              <ProgressBar className={StyleCard.progress} now={this.props.percent}  bsStyle={this.props.context} />;
            </div>
          );
        return(
            <div className={StyleCard.card}>
                <Row className={StyleCard.marginBottom}>
                    <div className={StyleCard.titleHability}>{this.props.hability}</div>
                    <div className={StyleCard.percent}>{this.props.percent}%</div>
                </Row>
                <Row>
                    {customProgressBarStyle}
                </Row>
            </div>
        );
    }
}
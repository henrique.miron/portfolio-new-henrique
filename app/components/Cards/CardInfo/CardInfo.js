import React,{Component} from 'react';
import {Col, Thumbnail} from 'react-bootstrap';
import StyleArtigos from './CardInfo.scss';

export default class CardInfo extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
				<Col xs={12} md={4}>
                    <div className={StyleArtigos.Content+" text-center"}>
                        <h3>{this.props.title}</h3>
                        <h4>{this.props.sub}</h4>
						<span className={StyleArtigos.line} ></span>
                    </div>
                </Col>
			)
	}
}
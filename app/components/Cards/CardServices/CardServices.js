import React,{Component} from 'react';
import {Col, Thumbnail} from 'react-bootstrap';
import Style from './CardServices.scss';

export default class cardArtigos extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
				<Col xs={6} md={4}>
                    <Thumbnail src="../assets/thumb-artigos.jpg" alt="242x200" className={Style.Thumbnail}>
                    <div className={Style.Content}>
                        <h4 class="text-center">{this.props.title}</h4>
                    </div>
                    </Thumbnail>
                </Col>
			)
	}
}
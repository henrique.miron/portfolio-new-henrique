import React,{Component} from 'react';
import {ProgressBar, Button, Col, Row} from 'react-bootstrap';
import StyleCard from './CardExperience.scss';


export default class CardExperience extends Component {
    
    render(){
        return(
            <div className={StyleCard.card}>
                <Row className={StyleCard.marginBottom}>
                    <div className={StyleCard.title}>{this.props.title}</div>
                </Row>
                <Row>
                    <div className={StyleCard.subtitle}>{this.props.subtitle}</div>                    
                </Row>
            </div>
        );
    }
}
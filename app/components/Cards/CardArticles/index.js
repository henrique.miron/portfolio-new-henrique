import React,{Component} from 'react';
import {Col, Thumbnail} from 'react-bootstrap';
import StyleArtigos from './index.scss';

export default class cardArtigos extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
				<Col xs={6} md={4}>
                    <Thumbnail src="../assets/thumb-artigos.jpg" alt="242x200" className={StyleArtigos.Thumbnail}>
                    <div className={StyleArtigos.Content}>
                        <h4>{this.props.title}</h4>
                        <p>Se você já é nosso cliente, ou conversou com a gente sobre seu site, possivelmente a palavra domínio surgiu durante nosso papo. Mas afinal de contas, o que é um domínio?...</p>
                        <a href="#">Ler artigo completo...</a>                            
                    </div>
                    </Thumbnail>
                </Col>
			)
	}
}
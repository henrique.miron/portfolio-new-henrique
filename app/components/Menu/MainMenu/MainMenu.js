import React,{Component} from 'react';
import BarMenu from '../BarMenu/BarMenu';
import {Media} from 'react-bootstrap';
import styleMenu from './MainMenu.scss';
import styleBar from '../BarMenu/BarMenu.scss';
export default class MainMenu extends Component {
    render(){
        return(
            <div className={'menu'}>
                <BarMenu/>
                <div className={styleMenu.mainMenu}>
                    <Media><img src={require('./../../../../assets/logo.png')} alt="Logo 4Pixels Agência Digital" className={styleMenu.logo+' img-responsive'} /></Media>
                    <ul className={styleBar.nav+' '+styleMenu.listMenu}>
                        <li className={styleBar.navItem+' '+styleMenu.menuItens}><a>HOME</a></li>
                        <li className={styleBar.navItem+' '+styleMenu.menuItens}><a>A AGÊNCIA</a></li>
                        <li className={styleBar.navItem+' '+styleMenu.menuItens}><a>SERVIÇOS</a></li>
                        <li className={styleBar.navItem+' '+styleMenu.menuItens}><a>CASES</a></li>
                        <li className={styleBar.navItem+' '+styleMenu.menuItens}><a>CONTATO</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}
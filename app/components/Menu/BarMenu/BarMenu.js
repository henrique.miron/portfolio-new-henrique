import React,{Component} from 'react';
import styleBar from './BarMenu.scss';
export default class BarMenu extends Component {

    render(){
        return(
            <ul className={styleBar.nav+' '+styleBar.navBorder}>
                <li className={styleBar.navItem}><a >Suporte</a></li>
                <li className={styleBar.navItem}><a><i class={'fab fa-facebook-f'}></i></a></li>
                <li className={styleBar.navItem}><a><i class={'fab fa-linkedin'}></i></a></li>
                <li className={styleBar.navItem}><a>En</a></li>
            </ul>
        )
    }
}
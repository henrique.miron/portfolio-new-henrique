import React,{Component} from 'react';
import {Grid,Row,Col,Media} from 'react-bootstrap';
import MainMenu  from '../Menu/MainMenu/MainMenu';
import StyleTopBar from './TopBar.scss';
export default class Teste extends Component {
    render(){
        return(
            <Grid bsClass={'container'}>
                <MainMenu/>
                <Row className={StyleTopBar.afterProfile}>
                    <Col md={7}>
                        <div className={StyleTopBar.text}>
                            <h4 className={StyleTopBar.titleExtraBold}>Diretor Executivo</h4>
                            <h1 className={StyleTopBar.titleExtraBold}>Roberto Gadducci</h1>
                            <p>Olá :) Meu nome é Roberto e eu sou o diretor executivo aqui da 4Pixels. Comecei a trabalhar com 16 anos, e já na área de desenvolvimento de sites, e desde então já acumulei experiência em todas as pontas do processo, desde código front-end e back-end, até gestão de projetos e os bastidores de ecommerces, inclusive em empresas fora do Brasil.</p>
                            <p>Conhecimento em diversas áreas, como Design de Navegação, Design de Informação, Linguagem Visual, Design Thinking entre outras. Atualmente atuo na área de gerencia de projetos, planejamento e comercial, além de programação front-end.</p>
                        </div>
                    </Col>
                </Row>
            </Grid>
        );
    }
}
import React,{Component} from 'react';
import {ProgressBar, Col, Row} from 'react-bootstrap';
import StyleCard from './MyHabilities.scss';
import CardHability from '../Cards/CardHability/CardHability';

export default class MyHabilities extends Component {

    getClass(context) {        
        switch(context) {
            case 'red' :
            return StyleCard.triangle + StyleCard.redTriangle;
        }
    }
    render() {
        var names = this.props.habilities;
        var namesList = names.map(function(habilitie){
                        return <li><CardHability hability={habilitie.name} percent={habilitie.percent} context={habilitie.context} /></li>;
                    });
        return (            
            <div className={StyleCard.habilities}>
                <div className={StyleCard.triangle + " " + StyleCard.redTriangle}></div>                            
                <h5>Minhas Habilidades</h5>
                {namesList}
            </div>
        );
    }
}

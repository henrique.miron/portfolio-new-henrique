import React,{Component} from 'react';
import ReactDom from 'react-dom';

import QuemSomosInterno from './pages/quemSomosInterno/quemSomosInterno';
import Contato from './pages/contato/contato';
import Cases from './pages/cases/cases';
import Servicos from './pages/servicos/servicos';
import { Switch, BrowserRouter, Route, browserHistory } from 'react-router-dom';

class App extends Component{
    render(){
        return(
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/public/contato' component={Contato}/>
                    <Route path='/public/quem-somos' component={QuemSomosInterno}/>
                </Switch>
            </BrowserRouter>
        )
    }
}

ReactDom.render(<Servicos />,document.getElementById('app'));

